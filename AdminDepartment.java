package week.com.assignment;

public class AdminDepartment  extends SuperDepartment{
	public String departmentName() {
		return "ADMIN DEPARTMENT";
	}
	public String getTodaysWork() {
		
		return "Complete your Documents Submission ";
	}
	
	public String getWorkDeadline() {
		
		return "Complete by EOD ";
	}
	public String isTodayAHoliday() {
		return "Today is not a holiday";
	}

}